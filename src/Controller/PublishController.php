<?php


namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\Update;

class PublishController
{
    /**
     * @param Publisher $publisher
     * @return Response
     */
    public function __invoke(Publisher $publisher): Response
    {
        $update = new Update(
            'http://localhost:8000/example.txt',
            json_encode(['message' => 'Bonjour William'])
        );

        $publisher($update);

        return new Response('published!');
    }
}